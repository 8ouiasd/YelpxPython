# YelpxPython

PyQt5 based Yelp GUI database tool that can load MySQL database from Amazon RDS 
cloud and search, display important information about users and business (map
function using Google Maps API) and based on user’s past ratings suggest a 
list of business he/she might be interested in. 

Demo video: https://www.youtube.com/watch?v=DHRymN6YgQk